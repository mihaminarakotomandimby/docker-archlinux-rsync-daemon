FROM archlinux:latest
RUN echo 'Server = https://mirror.dogado.de/archlinux/$repo/os/$arch             ' > /etc/pacman.d/mirrorlist
RUN echo 'Server = https://mirrors.niyawe.de/archlinux/$repo/os/$arch		 ' >> /etc/pacman.d/mirrorlist
RUN echo 'Server = https://packages.oth-regensburg.de/archlinux/$repo/os/$arch	 ' >> /etc/pacman.d/mirrorlist
RUN echo 'Server = https://mirror.ubrco.de/archlinux/$repo/os/$arch		 ' >> /etc/pacman.d/mirrorlist
RUN echo 'Server = https://phinau.de/arch/$repo/os/$arch			 ' >> /etc/pacman.d/mirrorlist
RUN echo 'Server = https://ftp.halifax.rwth-aachen.de/archlinux/$repo/os/$arch	 ' >> /etc/pacman.d/mirrorlist
RUN echo 'Server = https://mirror.bethselamin.de/$repo/os/$arch			 ' >> /etc/pacman.d/mirrorlist
RUN echo 'Server = https://arch.jensgutermuth.de/$repo/os/$arch			 ' >> /etc/pacman.d/mirrorlist
RUN echo 'Server = https://mirror.gnomus.de/$repo/os/$arch			 ' >> /etc/pacman.d/mirrorlist
RUN echo 'Server = https://ftp.fau.de/archlinux/$repo/os/$arch			 ' >> /etc/pacman.d/mirrorlist
RUN echo 'Server = https://mirror.wtnet.de/arch/$repo/os/$arch                   ' >> /etc/pacman.d/mirrorlist 


RUN pacman -Sy --noconfirm rsync curl wget supervisor
RUN mkdir /home/public_rsync && chown -R nobody.nobody /home/public_rsync && chmod -R 755 /home/public_rsync  
ADD ./rsyncd.conf /etc/rsyncd.conf
ADD ./supervisord.conf /etc/supervisord.conf

EXPOSE 873
ADD ./entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
